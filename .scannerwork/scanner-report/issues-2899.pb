a
phpS112FDefine and throw a dedicated exception instead of using a generic one. 2
�� $�	
phpS3776TRefactor this function to reduce its Cognitive Complexity from 51 to the 15 allowed. )      B@2
�� :
�
�� 
+1:
�
�� 
+1:
�
�� 
+1:
�
��S T+1:
�
��
 +1:
�
��[ \+1:
�
��
 +1:
�
�� 
+1:
�
��
 +1:
�
��^ _+1:
�
�� 
+1:
�
��' )+1:
�
�� 
+1:
�
��! #+1:
�
��D F+1:+
)�
�� +2 (incl. 1 for nesting):+
)�
�� +2 (incl. 1 for nesting):+
)�
�� +2 (incl. 1 for nesting):
�
��/ 1+1:+
)�
�� +2 (incl. 1 for nesting):
�
�� +1:
�
�� 
+1:
�
��! #+1:
�
�� 
+1:
�
��! #+1:
�
�� 
+1:
�
��& (+1:
�
��' (+1:
�
�� 
+1:
�
�� 
+1:+
)�
�� +2 (incl. 1 for nesting):
�
�� 
+1:+
)�
��= >+2 (incl. 1 for nesting):+
)�
�� +2 (incl. 1 for nesting):
�
�� +1:+
)�
��b c+3 (incl. 2 for nesting):+
)�
�� +3 (incl. 2 for nesting):
�
�� 
+1:+
)�
�� +2 (incl. 1 for nesting)�
phpS1192RDefine a constant instead of duplicating this literal ".admin-categories" 3 times. )      @2
��3 F:
�
��' :Duplication.:
�
��2 EDuplication.K
phpS1066/Merge this if statement with the enclosing one. 2
�� 