i
phpS1301MReplace this "switch" statement with "if" statements to increase readability. 2
�� �
phpS1142<This method has 5 returns, which is more than the 3 allowed. 2
�� :&
$�
�� "return" statement.:&
$�
�� "return" statement.:&
$�
�� "return" statement.:&
$�
�� "return" statement.:&
$�
�� "return" statement.�
phpS3776TRefactor this function to reduce its Cognitive Complexity from 20 to the 15 allowed. )      @2
�� :
�
�� 
+1:
�
�� 
+1:
�
�� 
+1:+
)�
�� +2 (incl. 1 for nesting):
�
��6 8+1:+
)�
�� +3 (incl. 2 for nesting):
�
�� +1:
�
�� 
+1:
�
��Y Z+1:
�
��9 ;+1:
�
��< =+1:
�
��] ^+1:
�
��= ?+1:
�
�� +1:+
)�
�� +2 (incl. 1 for nesting):
�
��& (+1�
phpS1192RDefine a constant instead of duplicating this literal "index.php?option=" 7 times. )      @2\\ ':
�kk #Duplication.:
�
�� 'Duplication.:
�
�� 'Duplication.:
�
�� 'Duplication.:
�
�� +Duplication.:
�
�� *Duplication.�
phpS1192GDefine a constant instead of duplicating this literal "&view=" 7 times. )      @2\\: B:
�kk6 >Duplication.:
�
��: BDuplication.:
�
��: BDuplication.:
�
��: BDuplication.:
�
��> FDuplication.:
�
��= EDuplication.�
phpS1192FDefine a constant instead of duplicating this literal ".data" 5 times. )      @2gg, 3:
�
��0 7Duplication.:
�
��0 7Duplication.:
�
��4 ;Duplication.:
�
��4 ;Duplication.